<?php

/**
 * @file
 * Administrative settings for Concurrent Editing Unlock module.
 */

/**
 * Form builder for administrative settings.
 */
function concurrent_editing_unlock_settings_form($form, &$form_state) {
  $form['concurrent_editing_unlock_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Concurrent Editing Unlock Settings'),
  );

  $form['concurrent_editing_unlock_settings']['concurrent_editing_unlock_enable'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enable Concurrent Editing Unlock on content types'),
    '#default_value' => variable_get('concurrent_editing_unlock_enable', array()),
    '#options' => node_type_get_names(),
    '#description' => t('Concurrent Editing Unlock will be enabled for users with the correct permissions on these content types.'),
  );

  return system_settings_form($form);
}

